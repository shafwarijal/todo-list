import Top from '@components/Top';
import List from '@components/List';
import Bottom from '@components/Bottom';

import PropTypes from 'prop-types';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { selectTheme } from '@containers/Settheme/selector';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import classes from './style.module.scss';

const Home = ({ theme: myTheme }) => {
  const isDark = myTheme === 'dark';

  const theme = createTheme({
    palette: {
      background: {
        default: isDark ? 'hsl(207, 26%, 17%)' : 'hsl(0, 0%, 98%)',
        paper: isDark ? 'hsl(237, 15%, 20%)' : 'hsl(0, 0%, 98%)',
      },
      text: {
        primary: isDark ? 'hsl(241, 19%, 90%)' : 'hsl(200, 15%, 8%)',
      },
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <div className={isDark ? classes.containerDark : classes.container}>
        <div className={isDark ? classes.containerImageDark : classes.containerImage} />
        <Top />
        <List />
        <Bottom />
      </div>
    </ThemeProvider>
  );
};

Home.propTypes = {
  theme: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
});

export default connect(mapStateToProps)(Home);
