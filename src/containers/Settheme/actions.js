import { SET_THEME } from '@containers/Settheme/constants';

export const setTheme = (theme) => ({
  type: SET_THEME,
  theme,
});
