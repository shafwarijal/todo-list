export const SET_TODO = 'Settodo/SET_TODO';
export const SET_TODO_FILTER = 'Settodo/SET_TODO_FILTER';
export const SET_TODO_ACTIVE = 'Settodo/SET_TODO_ACTIVE';
export const SET_TODO_REMOVE = 'Settodo/SET_TODO_REMOVE';
export const SET_TODO_CLEAR = 'Settodo/SET_TODO_CLEAR';
export const SET_TODO_ISEDITING = 'Settodo/SET_TODO_ISEDITING';
export const SET_TODO_EDIT = 'Settodo/SET_TODO_EDIT';
export const SET_TODO_DRAG = 'Settodo/SET_TODO_DRAG';
