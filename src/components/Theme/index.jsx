import PropTypes from 'prop-types';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import classes from './style.module.scss';

const propTypes = {
  children: PropTypes.element.isRequired,
  theme: PropTypes.string,
};

const Theme = ({ children, theme: myTheme }) => {
  const isDark = myTheme === 'dark';

  const theme = createTheme({
    palette: {
      background: {
        default: isDark ? 'hsl(207, 26%, 17%)' : 'hsl(0, 0%, 98%)',
        paper: isDark ? 'hsl(207, 26%, 17%)' : 'hsl(0, 0%, 98%)',
      },
      text: {
        primary: isDark ? 'hsl(0, 0%, 100%)' : 'hsl(200, 15%, 8%)',
      },
    },
  });
  return (
    <ThemeProvider theme={theme}>
      <div className={myTheme === 'dark' ? classes.darkTheme : classes.lightTheme}>{children}</div>
    </ThemeProvider>
  );
};

Theme.propTypes = propTypes;

export default Theme;
