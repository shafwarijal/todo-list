import classes from './style.module.scss';

const Bottom = () => <div className={classes.bottom}>Drag and drop to reorder list</div>;

export default Bottom;
